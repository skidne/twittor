function handleLikesAndRetweets () {
  const buttons = Array.from(document.getElementsByClassName('tweet-btn'))

  buttons.forEach(button => {
    const href = button.getAttribute('data-href')

    button.addEventListener('click', () => {
      if (!userAuthenticated) {
        window.location.href = 'accounts/login/'
        return
      }

      fetch(href).then(() => {
        const added = button.classList.toggle('toggled')
        const countSpan = button.querySelector('.count')
        const currentCount = parseInt(countSpan.innerText)
        const newCount = added ? currentCount + 1 : currentCount - 1
        countSpan.innerText = newCount.toString()
      })
    })
  })
}

function handleTweetButton () {
  const button = document.getElementById('tweet-submit')
  const textField = document.getElementById('tweet-content')

  textField.addEventListener('keyup', () => {
    if (textField.value.length > 0) button.disabled = false
    else button.disabled = true
  })
}
