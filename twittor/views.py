from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Tweet

def home(request):
    tweets = Tweet.objects.order_by('-posted_on')[:5]
    return render(request, 'home.html', { 'tweets': tweets })

@login_required
def tweet(request):
    if request.method == 'POST':
        content = request.POST.get('tweet_content', '')
        tweet = Tweet(author=request.user, content=content)
        tweet.save()
        return redirect('home')
    return render(request, 'create_tweet.html')

@login_required
def like_tweet(request, id):
    tweet = get_object_or_404(Tweet, pk=id)
    user = request.user
    if user in tweet.likes.all():
        tweet.likes.remove(user)
    else:
        tweet.likes.add(user)
    return redirect('home')

@login_required
def retweet(request, id):
    tweet = get_object_or_404(Tweet, pk=id)
    user = request.user
    if user in tweet.retweets.all():
        tweet.retweets.remove(user)
    else:
        tweet.retweets.add(user)
    return redirect('home')

