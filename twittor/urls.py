"""twittor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

from django.conf import settings
from django.conf.urls.static import static
from django_registration.backends.one_step.views import RegistrationView
from .forms import RegistrationForm

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('tweets/<int:id>/like', views.like_tweet, name='like'),
    path('tweets/<int:id>/retweet', views.retweet, name='retweet'),
    path('admin/', admin.site.urls),
    path('avatar/', include('avatar.urls')),
    path('accounts/register/', RegistrationView.as_view(form_class=RegistrationForm,
                                                        success_url='/'), name='register'),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('tweet/', views.tweet, name='tweet'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
