from django.db import models
from django.conf import settings

class FollowRelation(models.Model):
    followee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='followers')
    follower = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='followed_users')
    followed_on = models.DateTimeField(auto_now_add=True)
