from django.db import models
from django.conf import settings
from django.urls import reverse

from math import floor
import datetime

class Tweet(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='tweets')
    content = models.CharField(max_length=140)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='liked_tweets')
    retweets = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='retweets')
    posted_on = models.DateTimeField(auto_now_add=True)

    def like_url(self):
        return reverse('like', args=[self.id])

    def retweet_url(self):
        return reverse('retweet', args=[self.id])

    def formatted_posted_on(self):
        diff = datetime.datetime.utcnow() - self.posted_on.replace(tzinfo=None)
        s = diff.seconds
        if diff.days > 7 or diff.days < 0:
            return self.posted_on.strftime('%d %b %y')
        elif diff.days == 1:
            return f'1 day ago'
        elif diff.days > 1:
            return f'{diff.days} days ago'
        elif s <= 1:
            return f'just now'
        elif s < 60:
            return f'{s} seconds ago'
        elif s < 120:
            return f'1 minute ago'
        elif s < 3600:
            return f'{floor(s / 60)} minutes ago'
        elif s < 7200:
            return f'1 hour ago'
        else:
            return f'{floor(s / 3600)} hours ago'
