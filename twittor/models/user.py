from django.db import models
from django.contrib.auth.models import AbstractUser
from .follow_relation import FollowRelation


class User(AbstractUser):
    def add_follower(self, user):
        relation = FollowRelation(followee=self, follower=user)
        relation.save()
        return user

    def remove_follower(self, user):
        try:
            relation = FollowRelation.objects.get(followee=self, follower=user)
            if relation:
                relation.delete()
                return user
        except FollowRelation.DoesNotExist:
            return None

    def get_followers(self):
        relations = list(self.followers.all().select_related('follower'))
        return list(map(lambda relation: relation.follower, relations))

    def get_followed_users(self):
        relations = list(self.followed_users.all().select_related('followee'))
        return list(map(lambda relation: relation.followee, relations))
