from django import template

register = template.Library()


@register.inclusion_tag('components/tweet.html', takes_context=True)
def render_tweet(context, tweet):
    user = context['request'].user
    return { 'tweet': tweet, 'user': user }


@register.simple_tag
def formatted_name(user):
    if user.first_name and user.last_name:
        return f'{user.first_name} {user.last_name}'
    else:
        return user.username

@register.simple_tag
def liked(tweet, user):
    return user in tweet.likes.all()


@register.simple_tag
def retweeted(tweet, user):
    return user in tweet.retweets.all()
