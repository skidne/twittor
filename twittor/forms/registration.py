from django_registration import forms
from twittor.models import User

class RegistrationForm(forms.RegistrationForm):
    class Meta(forms.RegistrationForm.Meta):
        model = User
