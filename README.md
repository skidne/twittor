# twittor

Twitter clone. kinda.

# Setup

> Note: replace `pip` and `python` with `pip3` and `python3` for the commands below if you prefer.

1. Clone repository.
2. `cd` into repository directory.
3. Run `pip install -r requirements.txt`
4. Run `python manage.py migrate`
5. Run `python manage.py runserver`
